﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdminExamen
{
    class Student : Person
    {
        // PROPERTIES
        public static List<Student> List { get; set; }

        public int Age
        {
            get { return DateTime.Now.Year - Birthday.Year; }
        }



        public Student() {
        }

        public Student(string firstName, string lastName, DateTime birthday, int id, int schoolId, string contactNumber) 
            :base(firstName, lastName, birthday, id, schoolId, contactNumber) 
        {
        }

        // METHODEN
        public static string ShowAll()
        {
            string text = "Lijst van studenten: \n";
            foreach (var student in List)
            {
                text += $"{student.FirstName}, {student.LastName}, {student.Birthday:yyyy-M-dd}, {student.Id}, {student.SchoolId};\n";
            }
            return text;
        }

        public override string ShowOne()
        {
            return $"Gegevens van de student: {this.FirstName}, {this.LastName}, {this.Birthday:yyyy-M-dd}, {this.Id}, {this.SchoolId}.";
        }

        public override string GetNameTagText()
        {
            return $"(STUDENT) {this.FirstName} {this.LastName} ({this.ContactNumber})";
        }
    }

}
